use super::*;
use cgalgebra::vector3::Vec3;
use cgalgebra::vector3::dot;
use super::TESTVALUE;

#[derive(Serialize,Deserialize)]
pub struct Plane  {
	pub pos:Vec3,
	pub normal:Vec3
}
#[typetag::serde]
impl TestRay for Plane{
	fn test_ray(&self,r:&Ray,res:&mut TestResult) -> bool{
		
	  let o=r.o-self.pos;

	  let a=dot(&self.normal,&r.d);
	  if a< -TESTVALUE {
	    let b=dot(&self.normal,&o);
	    res.t=-b/a;
	    res.normal=self.normal;
	    true
	  }else{
	    res.t=0.0;
	    false
	  }
	}
}