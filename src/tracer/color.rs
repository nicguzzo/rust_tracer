use std::ops;
use std::fmt;
use std::f32;

#[derive(Debug,Copy,Clone,Serialize,Deserialize)]
pub struct Color{
  pub r:f32,
  pub g:f32,
  pub b:f32
}

#[macro_export]
macro_rules! color {
   ($r:expr,$g:expr,$b:expr) => {      
      Color{r:$r,g:$g,b:$b}      
  };
}

impl fmt::Display for Color {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "r: {}, g: {}, b: {}", self.r, self.g, self.b)
  }
}

impl Color{
  pub fn clamp(&mut self){
    if self.r > 1.0 { self.r = 1.0; }
    if self.g > 1.0 { self.g = 1.0; }
    if self.b > 1.0 { self.b = 1.0; }
  }  
  pub fn expose(&mut self,exposure:f32){
      self.r = 1.0 - (self.r * exposure).exp();
      self.g = 1.0 - (self.g * exposure).exp();
      self.b = 1.0 - (self.b * exposure).exp();
  }
  pub fn to_u32(self)->u32{    
    0xff    << 24 |
    (((self.r*255.0) as u32 )<<16)|
    (((self.g*255.0) as u32 )<<8)|
    (((self.b*255.0) as u32 ))
    
  }
}

impl ops::Add for Color{
  type Output = Color;
  fn add(self,other:Color)->Color{
      color!(
        self.r + other.r,
        self.g + other.g,
        self.b + other.b
      )
  }
}

impl ops::AddAssign for Color {
  fn add_assign(&mut self, other: Color) {
      *self = color!(
        self.r + other.r,
        self.g + other.g,
        self.b + other.b
      )
  }
}

impl ops::Sub for Color{
  type Output = Color;
  fn sub(self,other:Color)->Color{
      color!(
        self.r - other.r,
        self.g - other.g,
        self.b - other.b
      )
  }
}

impl ops::SubAssign for Color {
  fn sub_assign(&mut self, other: Color) {
      *self = color!(
        self.r - other.r,
        self.g - other.g,
        self.b - other.b
      )
  }
}
impl ops::Mul<Color> for Color{
type Output = Color;
fn mul(self,other:Color)->Color{
    color!(
       self.r * other.r,
       self.g * other.g,
       self.b * other.b
    )
}
}

impl ops::MulAssign<Color> for Color {
fn mul_assign(&mut self, other: Color) {
    *self = color!(
       self.r * other.r,
       self.g * other.g,
       self.b * other.b
    )
}
}

impl ops::Mul<f32> for Color{
  type Output = Color;
  fn mul(self,other:f32)->Color{
      color!(
         self.r * other,
         self.g * other,
         self.b * other
      )
  }
}

impl ops::MulAssign<f32> for Color {
  fn mul_assign(&mut self, other: f32) {
      *self = color!(
         self.r * other,
         self.g * other,
         self.b * other
      )
  }
}

impl ops::Div<f32> for Color{
  type Output = Color;
  fn div(self,other:f32)->Color{
      color!(
        self.r / other,
        self.g / other,
        self.b / other
      )
  }
}

impl ops::DivAssign<f32> for Color {
  fn div_assign(&mut self, other: f32) {
      *self = color!(
        self.r / other,
        self.g / other,
        self.b / other
      )
  }
}