use super::TESTVALUE;
use super::*;
use cgalgebra::vector3::Vec3;

#[derive(Serialize,Deserialize)]
pub struct Cylinder {
  pub pos: Vec3,
  pub radius: f32,
  pub height: f32,
  pub caps: bool,
}
#[typetag::serde]
impl TestRay for Cylinder {
  fn test_ray(&self, r: &Ray, res: &mut TestResult) -> bool {

    let o=r.o-self.pos;
    let a = r.d.x * r.d.x + r.d.z * r.d.z;
    let b = 2.0 * (r.d.x * o.x + r.d.z * o.z);
    let c = o.x * o.x + o.z * o.z - self.radius*self.radius;


    let disc = (b * b) - (4.0 * a * c);

    if  disc >= TESTVALUE {
      if  disc == TESTVALUE {
        res.t = (-b) / (2.0 * a);
        res.normal.x = 2.0 * (o.x + r.d.x * res.t);
        res.normal.y = 0.0;
        res.normal.z = 2.0 * (o.z + r.d.z * res.t);
        return true;
      } else {
        let root1 = (-b - disc.sqrt()) / (2.0 * a);
        let root2 = (-b + disc.sqrt()) / (2.0 * a);
        let tt = if root1 < root2 {
          res.t = root1;
          root2
        } else {
          res.t = root2;
          root1
        };
        let test = o.y + r.d.y * res.t;
        if test > self.height {
          if self.caps && (o.y + r.d.y * tt) <= self.height {
            res.normal.x = 0.0;
            res.normal.y = 1.0;
            res.normal.z = 0.0;

            let a = dot(&res.normal, &r.d);
            let b = dot(&res.normal, &o);
            res.t = -(b - 1.0) / a;
            return true;
          }
          res.t = 0.0;
          return false;
        }
        if test < 0.0 {
          if self.caps && (o.y + r.d.y * tt) >= 0.0 {
            res.normal.x = 0.0;
            res.normal.y = -1.0;
            res.normal.z = 0.0;

            let a = dot(&res.normal, &r.d);
            let b = dot(&res.normal, &o);
            res.t = -b / a;
            return true;
          }
          res.t = 0.0;
          return false;
        }
        res.normal.x = 2.0 * (o.x + r.d.x * res.t);
        res.normal.y = 0.0;
        res.normal.z = 2.0 * (o.z + r.d.z * res.t);
        return true;
      }
    } else {
      res.t = 0.0;
      return false;
    }
  }
}
