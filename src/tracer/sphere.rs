use super::*;
//use super::Test;
use super::TestResult;
use cgalgebra::vector3::Vec3;
use cgalgebra::vector3::dot;
use super::TESTVALUE;
use serde_derive::Deserialize;

#[derive(Serialize,Deserialize)]
pub struct Sphere{
	pub radius:f32,
	pub pos:Vec3
}
#[typetag::serde]
impl TestRay for Sphere{
  fn test_ray(&self,r:&Ray,res:&mut TestResult) -> bool{
  	
    let o=r.o-self.pos;
    let a=dot(&r.d,&r.d);
    let b=dot(&o,&r.d)*2.0;
    let c=dot(&o,&o)-self.radius*self.radius;

    let mut disc=(b*b)-(4.0*a*c);

    if disc>=TESTVALUE {
      if disc==TESTVALUE {
          res.t=(-b)/(2.0*a);
      }else{
        disc=disc.sqrt();
        let root1=(-b-disc)/(2.0*a);
        let root2=(-b+disc)/(2.0*a);
        if root1<root2 {
          res.t=root1;
        } else {
          res.t=root2;
        }
      };
      res.normal=o+r.d* res.t;
      res.normal.normalize();
      true
    }else {
      false
    }
  }
}