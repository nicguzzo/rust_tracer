use super::TESTVALUE;
use super::*;
use cgalgebra::vector3::{Vec3,cross,dot};

#[derive(Serialize,Deserialize)]
pub struct Triangle {
  pub v1: Vec3,
  pub v2: Vec3,
  pub v3: Vec3,
  pub normal: Vec3
}
#[typetag::serde]
impl TestRay for Triangle {
  fn prepare(&mut self) {
    let v1 = self.v2 - self.v1;
    let v2 = self.v3 - self.v1;
    self.normal= cross(&v1,&v2).normalized();
    //println!("normal= {}",self.normal);
  }
  
  fn test_ray(&self, r: &Ray, res: &mut TestResult) -> bool {    
    let v1 = self.v2 - self.v1;
    let v2 = self.v3 - self.v1;
    let pvec = cross(&r.d,&v2);
    let det = dot(&v1,&pvec);

    // culling
    if det < TESTVALUE { return false }
    //non culling
    //if det.abs() < TESTVALUE { return false }  

    let inv_det = 1.0 / det;

    let tvec = r.o - self.v1;

    res.u = dot(&tvec,&pvec) * inv_det;

    if res.u < 0.0 || res.u > 1.0 { return false }

    let qvec = cross(&tvec,&v1);
    
    res.v = dot(&r.d,&qvec) * inv_det;
    
    if res.v < 0.0 || res.u + res.v > 1.0 { return false }
    
    res.t = dot(&v2,&qvec) * inv_det;
    res.normal=self.normal;
    
    true
  }
  /*
  fn test_ray(&self, r: &Ray, res: &mut TestResult) -> bool {    
    let a = dot(&self.normal, &r.d);
    if a < -TESTVALUE {    
      let b = dot(&self.normal, &r.o);
      res.t = -(b + self.dist) / a;
      if res.t > 0.0 {
        let (mut ps, mut pt, mut pu) = match self.axis {
          1 => {
            let ppy = r.o.y + r.d.y * res.t;
            let ppz = r.o.z + r.d.z * res.t;
            let t1y = self.v3.y - ppy;
            let t1z = self.v3.z - ppz;
            let t2y = self.v2.y - ppy;
            let t2z = self.v2.z - ppz;
            let t3y = self.v1.y - ppy;
            let t3z = self.v1.z - ppz;
            (
              (t1y * t2z - t1z * t2y) * self.normal.x,
              (t3y * t1z - t3z * t1y) * self.normal.x,
              (t2y * t3z - t2z * t3y) * self.normal.x,
            )
          }
          2 => {
            let ppx = r.o.x + r.d.x * res.t;
            let ppz = r.o.z + r.d.z * res.t;
            let t1x = self.v3.x - ppx;
            let t1z = self.v3.z - ppz;
            let t2x = self.v2.x - ppx;
            let t2z = self.v2.z - ppz;
            let t3x = self.v1.x - ppx;
            let t3z = self.v1.z - ppz;
            (
              (t1z * t2x - t1x * t2z) * self.normal.y,
              (t3z * t1x - t3x * t1z) * self.normal.y,
              (t2z * t3x - t2x * t3z) * self.normal.y,
            )
          }
          3 => {
            let ppx = r.o.x + r.d.x * res.t;
            let ppy = r.o.y + r.d.y * res.t;
            let t1x = self.v3.x - ppx;
            let t1y = self.v3.y - ppy;
            let t2x = self.v2.x - ppx;
            let t2y = self.v2.y - ppy;
            let t3x = self.v1.x - ppx;
            let t3y = self.v1.y - ppy;
            (
              (t1x * t2y - t1y * t2x) * self.normal.z,
              (t3x * t1y - t3y * t1x) * self.normal.z,
              (t2x * t3y - t2y * t3x) * self.normal.z,
            )
          }
          _ => (0.0, 0.0, 0.0),
        };

        if pu.signum() == -1.0 {
          pu = -1.0
        };
        if ps.signum() == -1.0 {
          ps = -1.0
        };
        if pt.signum() == -1.0 {
          pt = -1.0
        };

        if (pu >= 0.0) && (ps >= 0.0) && (pt >= 0.0) {
          res.normal = self.normal;
          return true;
        }
      }
    }
    res.t = 0.0;
    false
  }*/
}

