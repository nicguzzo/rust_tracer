use super::*;
use cgalgebra::vector3::Vec3;
use super::TESTVALUE;
use std::mem;
use std::f32::INFINITY;
use std::f32::NEG_INFINITY;

#[derive(Serialize,Deserialize)]
pub struct Cube  {
  pub pos1:Vec3,
  pub pos2:Vec3
}

#[typetag::serde]
impl TestRay for Cube{  
  fn test_ray(&self,r:&Ray,res:&mut TestResult) -> bool{
    let Cube{pos1,pos2}=self;
    let  (mut t1,mut t2)=if  r.d.x.abs() < TESTVALUE {      
      if (r.o.x<pos1.x) || (r.o.x> pos2.x) {
        return false
      }
      (pos1.x-r.o.x, pos2.x-r.o.x)
    }else{
      ( (pos1.x-r.o.x)/r.d.x, (pos2.x-r.o.x)/r.d.x )
    };
    let mut tnear = NEG_INFINITY;
    let mut tfar = INFINITY;
    let mut s_in=0;
    let mut _s_out=1;
    if t1>t2 { 
      mem::swap(&mut t1,&mut t2);
    }
    if t1>tnear{ 
      tnear=t1; 
      if r.o.x>=0.0{
         s_in=0;
      }else{ 
        s_in=1;
      }
    }
    if t2<tfar { 
      tfar=t2;  
      if r.o.x>=0.0 { 
        _s_out=1;
      }else{ 
        _s_out=0;
      }
    }

    if (tnear>tfar) || (tfar <0.0) { 
      return false 
    }

    if r.d.y.abs()<TESTVALUE
    {
      if (r.o.y<pos1.y) || (r.o.y> pos2.y){
        return false
      }
      t1=pos1.y-r.o.y;
      t2=pos2.y-r.o.y;
    }else{
      t1=(pos1.y-r.o.y)/r.d.y;
      t2=(pos2.y-r.o.y)/r.d.y;
    }

    if t1>t2 { 
      mem::swap(&mut t1,&mut t2);
    }
    if t1>tnear{ 
      tnear=t1; 
      if r.o.y>=0.0 { 
        s_in=2; 
      } else { 
        s_in=3;
      }
    }
    if t2<tfar { 
      tfar=t2;  
      if r.o.y>=0.0 {
        _s_out=3;
      }else{ 
        _s_out=2;
      }
    }

    if (tnear>tfar) || (tfar <0.0){
      return false;  
    }

    if r.d.z.abs() < TESTVALUE {
      if (r.o.z<pos1.z) || (r.o.z> pos2.z) { 
        return false;
      }
      t1=pos1.z-r.o.z;
      t2=pos2.z-r.o.z;
    }else{
      t1=(pos1.z-r.o.z)/r.d.z;
      t2=(pos2.z-r.o.z)/r.d.z;
    }


    if t1>t2 { 
      mem::swap(&mut t1,&mut t2);
    }
    if t1>tnear { 
      tnear=t1; 
      if r.o.z>=0.0 {
        s_in=4; 
      }else{ 
        s_in=5;
      }
    }
    if t2<tfar{ 
      tfar=t2;  
      if r.o.z>=0.0 {
        _s_out=5; 
      }else{
        _s_out=4;
      }
    }

    if (tnear>tfar) || (tfar <0.0) {
      return false;  
    }

    res.t=tnear;

    match s_in{
      0 =>  res.normal=vec3!( 1.0, 0.0, 0.0),
      1 =>  res.normal=vec3!(-1.0, 0.0, 0.0),
      2 =>  res.normal=vec3!( 0.0, 1.0, 0.0),
      3 =>  res.normal=vec3!( 0.0,-1.0, 0.0),
      4 =>  res.normal=vec3!( 0.0, 0.0, 1.0),
      5 =>  res.normal=vec3!( 0.0, 0.0,-1.0),
      _ =>()
    }    
    true
  }
}
