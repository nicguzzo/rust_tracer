pub mod cube;
pub mod cylinder;
pub mod plane;
pub mod sphere;
pub mod triangle;
#[macro_use]
pub mod color;
use sphere::Sphere;
use cgalgebra::vector3::dot;
use cgalgebra::vector3::Vec3;
use color::Color;
use std::f32::INFINITY;
//use cgalgebra::vector4::Vec4;
use serde::{Deserialize,Deserializer};
use std::any::Any;
pub const TESTVALUE: f32 = 1e-8;
//pub const MAXREC: i32 = 5;

#[derive(Serialize,Deserialize)]
pub struct Camera {
    pub eye: Vec3,
    pub look_at: Vec3,
    pub up: Vec3,
    pub aspect: f32,
    pub near: f32,
    pub far: f32,
    pub fov: f32,
}
#[derive(Debug)]
pub struct Ray {
    pub o: Vec3, //origin
    pub d: Vec3, //direction
}
pub struct TestResult {
    pub t: f32,
    pub u: f32,
    pub v: f32,
    pub normal: Vec3,
}

#[derive(Serialize,Deserialize)]
pub struct Material {
    pub name: String,
    pub diffuse: Color,
    pub ambient: Color,
    pub specular: Color,
    pub diffuse_q: f32,
    pub ambient_q: f32,
    pub specular_q: f32,
    pub reflection: f32,
    pub reflraction: f32,
    pub ior: f32,
    pub transparency: f32,
    pub shininess: f32,
}
#[derive(Serialize,Deserialize)]
pub struct Light {
    pub pos: Vec3,
    pub color: Color,
    pub att_k: f32,
    pub att_l: f32,
    pub att_q: f32,
}

#[typetag::serde(tag = "type")]
pub trait TestRay: Sync {
    fn test_ray(&self, ray: &Ray, res: &mut TestResult) -> bool;
    fn prepare(&mut self) {}    
}

#[derive(Serialize,Deserialize)]
pub struct Object {
    pub material: usize,
    pub primitive: Box<dyn TestRay>,
}

#[derive(Serialize,Deserialize)]
pub struct Scene {
    pub width: u32,
    pub height: u32,
    pub max_depth: u32,
    pub antialias: u32,
    pub background: Color,
    pub camera: Camera,
    pub materials: Vec<Material>,
    pub lights: Vec<Light>,
    //#[serde(skip_deserializing, default = "Vec::<Object>::new")]    
    pub objects: Vec<Object>,
}

pub fn trace(scene: &Scene, ray: &Ray, n: u32) -> Color {
    let mut res = TestResult {
        t: 0.0,
        u: 0.0,
        v: 0.0,
        normal: vec3!(0.0, 0.0, 0.0),
    };
    if let Some(ob_index) = find_intersections(&scene, &ray, &mut res) {
        let object = &scene.objects[ob_index];
        if object.material >= scene.materials.len() {
            //println!("invalid material");
            return scene.background;
        }
        let material = &scene.materials[object.material];
        let hit_point = ray.o + ray.d * res.t;
        let p2 = ray.o + ray.d * (res.t - 0.000001);
        let mut color = calculate_color(
            &scene,
            &hit_point,
            &res.normal,
            &ray.o,
            &p2,
            &material,
            ob_index,
        );
        //Reflected Ray
        if (material.reflection > 0.0) && (n < scene.max_depth) {
            let dir = (ray.o - hit_point).normalized();
            let ang = 2.0 * dot(&res.normal, &dir);
            let r = ((res.normal * ang) - dir).normalized();
            let refray = Ray { o: hit_point, d: r };
            let reflected = trace(&scene, &refray, n + 1);
            color += reflected * material.reflection;
            //expose(&mut color,-1.0);
            //clamp(&mut color);
        }
        //Refracted Ray
        /*if( (mat.transparency > 0.0f) && (n<MAXREC)){
            if(mat.ior==0.0f)
                mat.ior=1.0f;
            float index=amb_ref_index/mat.ior;
            k=material->transparency;

            float cosI=-(Normal*ray.d);
            float cosT2=1.0f-index*index*(1.0f-cosI*cosI);

            if(cosT2>0.0f)
            {
                dd=index * cosI-sqrtf(cosT2);
                T.x=ray.d.x*index - Normal.x*dd;
                T.y=ray.d.y*index - Normal.y*dd;
                T.z=ray.d.z*index - Normal.z*dd;

                fnormalize(T);
                Ray refray(hit_point+T*0.01,T);
                refracted=Trace(refray,n+1);

                colour.r=colour.r+k*refracted.r;
                colour.g=colour.g+k*refracted.g;
                colour.b=colour.b+k*refracted.b;
                colour.expose();
                //colour.clamp();
            }
        }*/
        color
    } else {
        //if n==0 {
        scene.background
        //}else{
        //    vec4!(0.0,0.0,0.0,0.0)
        //}
    }
}

fn find_intersections(scene: &Scene, ray: &Ray, mut res: &mut TestResult) -> Option<usize> {
    let mut t = INFINITY;
    let mut ob: Option<usize> = None;
    let mut i = 0;
    let mut res2 = TestResult {
        t: 0.0,
        u: 0.0,
        v: 0.0,
        normal: vec3!(0.0, 0.0, 0.0),
    };
    for obj in &scene.objects {
        if obj.primitive.test_ray(&ray, &mut res2) {
            if res2.t > 0.0 && res2.t < t {
                t = res2.t;
                res.t = res2.t;
                res.normal = res2.normal;
                ob = Some(i);
            }
        };
        i += 1;
    }
    ob
}

fn calculate_color(
    scene: &Scene,
    hit_point: &Vec3,
    normal: &Vec3,
    eye: &Vec3,
    p2: &Vec3,
    material: &Material,
    ob_index: usize,
) -> Color {
    let mut sum = color!(0.0, 0.0, 0.0);
    let v = (*eye - *hit_point).normalized();
    for light in &scene.lights {
        let mut l = light.pos - *hit_point;
        let len = l.length();
        l.normalize();
        let shadowray = Ray { o: *p2, d: l };
        if !testshadow(&scene, shadowray, len, ob_index) {
            sum += phong(&v, &l, &normal, &light, &material);
        }
    }
    let color = material.ambient * material.ambient_q * material.diffuse + sum;
    color
}

pub fn testshadow(scene: &Scene, ray: Ray, len: f32, ob_index: usize) -> bool {
    let mut res = TestResult {
        t: 0.0,
        u: 0.0,
        v: 0.0,
        normal: vec3!(0.0, 0.0, 0.0),
    };
    let mut i = 0;
    for obj in &scene.objects {
        if ob_index != i {
            if obj.primitive.test_ray(&ray, &mut res) {
                //let dst=ray.d*(res.t-0.000001);
                let dst = ray.d * res.t;
                let dist = dst.length();
                if res.t > 0.0 && dist < len {
                    return true;
                }
            }
        }
        i += 1;
    }
    false
}

fn phong(v: &Vec3, l: &Vec3, normal: &Vec3, light: &Light, material: &Material) -> Color {
    let dist = (*v - *l).length();
    let l = l.normalized();
    let mut cosang = dot(normal, &l);
    let r = (*normal * (2.0 * cosang) - l).normalized();
    let mut cosang2 = dot(&r, v);
    if cosang < 0.0 {
        cosang = 0.0;
    }
    if cosang2 < 0.0 {
        cosang2 = 0.0;
    }
    let q = light.att_k + light.att_l * dist + light.att_q * dist * dist;
    let mut fatt = 1.0;
    if q != 0.0 {
        fatt = 1.0 / q;
    }
    if fatt > 1.0 {
        fatt = 1.0;
    }
    let mut spec = 0.0;
    let diff = material.diffuse_q * cosang;
    if material.specular_q > 0.0 {
        spec = material.specular_q * cosang2.powf(material.shininess);
    }
    let color = light.color * fatt * material.diffuse * diff + material.specular * spec;
    color
}
