#[macro_use]
extern crate serde_derive;
extern crate json5;
#[macro_use]
extern crate cgalgebra;
#[macro_use]
mod tracer;
use cgalgebra::vector3;
use cgalgebra::vector3::Vec3;
use std::f32::consts::PI;
use std::fs;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;
use tracer::color::Color;
use tracer::cube::Cube;
use tracer::cylinder::Cylinder;
use tracer::plane::Plane;
use tracer::sphere::Sphere;
use tracer::triangle::Triangle;
use tracer::Object;

use rayon::prelude::*;

use tracer::*;

use std::time::Instant;



fn main()/* -> Result<(), Error>*/ {
    let f = fs::read_to_string("data/scene.json5").expect("no scene file");

    let mut scene: Scene = match json5::from_str(f.as_str()) {
        
        Ok(scn) => scn,
        Err(e) => {
            println!("scene error, invalid json, {}", e);
            Scene {
                //empty scene
                width: 600,
                height: 600,
                antialias: 2,
                background: color!(0.0, 0.0, 0.0),
                camera: Camera {
                    eye: vec3!(0.0, 0.0, -5.0),
                    look_at: vec3!(0.0, 0.0, 0.0),
                    up: vec3!(0.0, 1.0, 0.0),
                    aspect: 1.0,
                    near: 2.0,
                    far: 50.0,
                    fov: 60.0,
                },
                materials: Vec::<Material>::new(),
                objects: Vec::<Object>::new(),
                lights: Vec::<Light>::new(),
                max_depth: 5,
            }
        }
    };
   

    let width = scene.width;
    let height = scene.height;
    scene.camera.aspect = width as f32 / height as f32;
    let n = ((scene.camera.look_at - scene.camera.eye) * -1.0).normalized();
    let u = vector3::cross(&scene.camera.up, &n).normalized();
    let v = vector3::cross(&u, &n);

    let ch = scene.camera.near * (scene.camera.fov * 0.5 * (PI / 180.0)).tan();
    let cw = scene.camera.aspect * ch;
    let aa = scene.antialias; // super sampling antialiasing
    let mut inc = 1.0;
    let mut avg = 1.0;
    if aa != 0 {
        let aa = aa as f32;
        inc = 1.0 / aa;
        avg = 1.0 / (aa * aa);
    }

    let w = width as f32;
    let h = height as f32;
    
    let now = Instant::now();

    //let lines=Vec::<Line>::new();
    //for y in 0..height {
    //let lines: Vec<Vec<u8>> = (0..height) .map(|y| {
    let lines: Vec<Vec<u8>> = (0..height).into_par_iter().map(|y| {
            let mut line = Vec::<u8>::new();
            let mut ray = Ray {
                o: scene.camera.eye,
                d: vec3!(0.0, 0.0, 1.0),
            };
            for x in 0..width {
                let mut col = color!(0.0, 0.0, 0.0);
                if aa == 0 {
                    let uc = cw * (((2.0 * x as f32) / (w - 1.0)) - 1.0);
                    let vr = ch * (((2.0 * y as f32) / (h - 1.0)) - 1.0);
                    ray.d = (n * (-scene.camera.near)) + (u * uc) + (v * vr);
                    ray.d.normalize();
                    col = tracer::trace(&scene, &ray, 0);
                } else {
                    //super sampling loops
                    let aax = x as f32 + 1.0;
                    let aay = y as f32 + 1.0;
                    let mut xaa = x as f32;
                    while xaa < aax {
                        let mut yaa = y as f32;
                        while yaa < aay {
                            let uc = cw * (((2.0 * xaa) / (w - 1.0)) - 1.0);
                            let vr = ch * (((2.0 * yaa) / (h - 1.0)) - 1.0);
                            ray.d = (n * (-scene.camera.near)) + (u * uc) + (v * vr);
                            ray.d.normalize();
                            col += tracer::trace(&scene, &ray, 0);
                            yaa += inc
                        }
                        xaa += inc
                    }
                    col *= avg;
                }
                col.clamp();
                line.push((col.r * 255.0) as u8);
                line.push((col.g * 255.0) as u8);
                line.push((col.b * 255.0) as u8);
            }
            line
        })
        .collect();
    let millis = now.elapsed().as_millis();
    println!(
        "Rendered in: {} milli seconds, ({} seconds)",
        millis,
        millis as f32 / 1000.0
    );
    let buf_size = (width * height) as usize;
    let mut buf: Vec<u8> = Vec::with_capacity(buf_size * 3);
    lines
        .into_iter()
        .for_each(|line| line.into_iter().for_each(|x| buf.push(x)));
    write_image(width, height, &buf);
   
}

fn write_image(width: u32, height: u32, buf: &Vec<u8>) {
    let path = Path::new(r"trace.png");
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width, height);
    encoder.set_color(png::ColorType::RGB);
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(&buf).unwrap();
}
